﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Theater
    {
        public int TheaterId { get; set; }
        public string TheaterName { get; set; }
        
        
        //Constructor
        public Theater(int theaterId, string theaterName)
        {
            TheaterId = theaterId;
            TheaterName = theaterName;
        }
    }
}
