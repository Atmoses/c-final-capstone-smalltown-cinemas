﻿using Capstone.DAO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class MovieLoader
    {
        public static List<Movie> ImportMovieInfo(IShowingDAO showingDAO, IMovieDAO movieDAO)
        {
           List<Movie> MovieList = new List<Movie>();
            Dictionary<string, int> MovieDictionary = new Dictionary<string, int>();

            string filePath = @"movie-times-update-2.csv";

            Console.WriteLine(Directory.GetCurrentDirectory());

            try
            {
                using (StreamReader fileReader = new StreamReader(filePath))
                {
                    while (!fileReader.EndOfStream)
                    {
                        string input = fileReader.ReadLine();
                        string[] fields = input.Split(";");
                        int theaterId = Convert.ToInt32(fields[0]);
                        string movieTitle = fields[1];
                        int duration = Convert.ToInt32(fields[2]);
                        string movieRating = fields[3];
                        string startTime = Convert.ToString(fields[4]);
                        string filmDescription = fields[5];
                        string posterURL = fields[6];

                        Movie movie = new Movie(movieTitle, duration, movieRating, filmDescription, posterURL, theaterId);
                        MovieList.Add(movie);

                        int movieId = 0;

                        if (MovieDictionary.ContainsKey(movie.MovieTitle))
                        {
                            movieId = MovieDictionary[movie.MovieTitle];

                        }
                        else
                        {
                            // call the DAO to add the movie and capture the id into movieId 
                            movieId = movieDAO.InsertMovie(movie);

                            // add the movie and it's id to the dictionary
                            MovieDictionary.Add(movie.MovieTitle, movieId);
                        }

                        // today's date with the showing time
                       DateTime todaysShowing = DateTime.Parse(startTime);

                        // add the showtime for the next 7 days
                        for (int day = 0; day <= 7; day++)
                        {
                            Showing showing = new Showing(movieId, todaysShowing.AddDays(day));

                            // call the DAO to add to the database
                            showingDAO.InsertShowing(showing);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw;
            }

            return MovieList;
        }
    }
}

