﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Showing
    {
        public int ShowingId { get; set; }
        public DateTime ShowDateTime { get; set; }
        public int MovieId { get; set; }

        public Showing(int movieId, DateTime showDateTime)
        {
            MovieId = movieId;
            ShowDateTime = showDateTime;
            
        }

        public Showing(int movieId)
        {
            MovieId = movieId;
        }

        public bool IsMatinee
        {
            get
            {
                return ShowDateTime.Hour < 12; //returns true if before noon, false if after noon
            }
        }
    
        //public string ShowTimeString
        //{
        //    get
        //    {
        //        return ShowTime.ToString("hh:mm tt");
        //    }
        //}
    }
}
