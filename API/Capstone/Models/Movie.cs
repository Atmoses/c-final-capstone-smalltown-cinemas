﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public string MovieRating { get; set; }
        public int Duration { get; set; }
        public string FilmDescription { get; set; }
        public string PosterURL { get; set; }
        public int TheaterId { get; set; }

        public Movie(string movieTitle, int duration, string movieRating, string filmDescription, string posterURL, int theaterId)
        {
            MovieTitle = movieTitle;
            Duration = duration;
            MovieRating = movieRating;
            FilmDescription = filmDescription;
            PosterURL = posterURL;
            TheaterId = theaterId;
        }

        public Movie()
        {

        }
    }
}
