﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Ticket
    {
        public int TicketId { get; set; }
        public int SeatId { get; set; }
        public int RowNumber { get; set; }
        public int SeatNumber { get; set; }
        public int ShowingId { get; set; }
        public bool IsAdult { get; set; }
        public int UserId { get; set; }
    }
}
