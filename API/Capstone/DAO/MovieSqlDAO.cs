﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Models;
using System.Data.SqlClient;

namespace Capstone.DAO
{
    public class MovieSqlDAO : IMovieDAO
    {
        public readonly string connectionString;

        public MovieSqlDAO(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }
        
        //GET A LIST OF ALL THE MOVIES SHOWING AT THE CINEMA
        public List<Movie> GetMovies()
        {
            List<Movie> listOfAllMovies = new List<Movie>();

            string sql = "SELECT * FROM movie";
            //TODO: Would be great to not select from the whole table, in case we have multiple movvies and not all are showing rn. Maybe limit the search based on show_dateTime
            //string sql = "SELECT * FROM movie m JOIN showing sh ON m.movie_id = sh.movie_id WHERE sh.theater_id IN (1, 2, 3, 4)"; << for GetCurrentMovies?

            //connect to DB & execute query
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //open the connection
                    conn.Open();

                    //create the SQL command
                    SqlCommand cmd = new SqlCommand(sql, conn);

                    //execute the statement and get a reader
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Movie movie = GetMovieFromReader(reader);
                        
                        listOfAllMovies.Add(movie);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return listOfAllMovies;
        }

        private Movie GetMovieFromReader(SqlDataReader reader)
        {
            Movie movie = new Movie();

            movie.MovieTitle = Convert.ToString(reader["movie_title"]);
            movie.Duration = Convert.ToInt32(reader["duration"]);
            movie.MovieRating = Convert.ToString(reader["rating"]);
            movie.FilmDescription = Convert.ToString(reader["film_description"]);
            movie.PosterURL = Convert.ToString(reader["poster_url"]);
            movie.TheaterId = Convert.ToInt32(reader["theater_id"]);
            movie.MovieId = Convert.ToInt32(reader["movie_id"]);
            
            return movie;
        }

        //VIEW THE MOVIE AT A SINGLE THEATER
        public Movie GetTheaterMovie(int theaterId)
        {
            Movie movie = new Movie();

            string sql = $"SELECT * FROM movie WHERE theater_id = @theaterId";

            //connect to DB & execute query
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //open the connection
                    conn.Open();

                    //create the SQL command
                    SqlCommand cmd = new SqlCommand(sql, conn);

                    //bind the parameters
                    cmd.Parameters.AddWithValue("@theaterId", theaterId);

                    //execute the statement and get a reader
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        movie = GetMovieFromReader(reader);
                        
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return movie;
        }

        //INSERT A MOVIE INTO THE DB
        public int InsertMovie(Movie movie) //needs to return a movieiD
        {
            int movieId = 0;

            string sql = $"INSERT INTO movie (movie_title, duration, rating, film_description, poster_url, theater_id) VALUES (@movieTitle, @duration, @movieRating, @filmDescription, @posterURL, @theaterId); SELECT @@IDENTITY";
            
            //connect to DB & execute query
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //open the connection
                    conn.Open();

                    //create the SQL command
                    SqlCommand cmd = new SqlCommand();

                    //bind parameters
                    cmd.Parameters.AddWithValue("@movieTitle", movie.MovieTitle);
                    cmd.Parameters.AddWithValue("@duration", movie.Duration);
                    cmd.Parameters.AddWithValue("@movieRating", movie.MovieRating);
                    cmd.Parameters.AddWithValue("@filmDescription", movie.FilmDescription);
                    cmd.Parameters.AddWithValue("@posterURL", movie.PosterURL);
                    cmd.Parameters.AddWithValue("@theaterId", movie.TheaterId);

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Connection = conn;

                    //execute the command
                    movieId = Convert.ToInt32(cmd.ExecuteScalar());

                    //close the connection
                    conn.Close();

                    //referenced the following link:
                    // https://docs.microsoft.com/en-us/visualstudio/data-tools/insert-new-records-into-a-database?view=vs-2019
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return movieId;
        }
    }
}
