﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Models;

namespace Capstone.DAO
{
    public interface IShowingDAO
    {
        List<Showing> GetMovieShowings(int movieId);
        int InsertShowing(Showing showing);
    }
}
