﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Models;

namespace Capstone.DAO
{
    public interface IMovieDAO
    {
        List<Movie> GetMovies();
        Movie GetTheaterMovie(int theaterId);
        int InsertMovie(Movie movie);
    }
}
