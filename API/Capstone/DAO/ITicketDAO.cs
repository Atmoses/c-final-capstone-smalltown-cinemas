﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Models;

namespace Capstone.DAO
{
    public interface ITicketDAO
    {
        List<Ticket> GetAvailableTickets(int showingId);
        bool PurchaseTickets(Ticket ticket);
    }
}
