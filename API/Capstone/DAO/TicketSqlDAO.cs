﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Models;
using System.Data.SqlClient;

namespace Capstone.DAO
{
    public class TicketSqlDAO : ITicketDAO
    {
        public readonly string connectionString;

        public TicketSqlDAO(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }

        //RETURN A LIST OF AVAILABLE TICKETS
        public List<Ticket> GetAvailableTickets(int showingId)
        {
            List<Ticket> availableTickets = new List<Ticket>();

            //Select those tickets in the theater_seats table that are not in the SOLD tickets ticket table.
            string sql = @"SELECT t.*, 0 as ticket_id, s.showing_id, 0 as is_adult, 0 as user_id FROM theater_seats t
                        JOIN movie m ON t.theater_id = m.theater_id
                        JOIN showing s ON s.movie_id = m.movie_id
                        WHERE s.showing_id = @showingId
                        AND t.seat_id NOT IN (SELECT seat_id from ticket WHERE showing_id = @showingId)";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@showingId", showingId);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Ticket ticket = new Ticket();

                        ticket.TicketId = Convert.ToInt32(reader["ticket_id"]);
                        ticket.SeatId = Convert.ToInt32(reader["seat_id"]);
                        ticket.RowNumber = Convert.ToInt32(reader["row_number"]);
                        ticket.SeatNumber = Convert.ToInt32(reader["seat_number"]);
                        ticket.ShowingId = Convert.ToInt32(reader["showing_id"]);
                        ticket.IsAdult = Convert.ToBoolean(reader["is_adult"]);
                        ticket.UserId = Convert.ToInt32(reader["user_id"]);

                        availableTickets.Add(ticket);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return availableTickets;
        }

        //CREATE A METHOD THAT UPDATES THE TICKETS SOLD/THE NUMBER OF SEATS AVAILABLE/AND ANY BOOLS WHEN A TICKET IS SOLD
        public bool PurchaseTickets(Ticket ticket)
        {
            string sql = "INSERT INTO ticket (seat_id, showing_id, is_adult, user_id) VALUES (@seatId, @showingId, @isAdult, @userId)";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@seatId", ticket.SeatId);
                    cmd.Parameters.AddWithValue("@showingId", ticket.ShowingId);
                    cmd.Parameters.AddWithValue("@isAdult", ticket.IsAdult);
                    cmd.Parameters.AddWithValue("@userId", ticket.UserId);
                    
                    int rowsAffected = cmd.ExecuteNonQuery();

                    return (rowsAffected > 0);
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
    }
} 
