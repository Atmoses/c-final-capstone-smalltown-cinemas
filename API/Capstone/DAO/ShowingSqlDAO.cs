﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Models;
using System.Data.SqlClient;

namespace Capstone.DAO
{
    public class ShowingSqlDAO : IShowingDAO
    {
        public readonly string connectionString;
        private Movie movie = new Movie();

        public ShowingSqlDAO(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }

        //RETURN A LIST OF THE SHOWINGS FOR A PARTICULAR MOVIE FOR THE NEXT 7 DAYS
        public List<Showing> GetMovieShowings(int movieId)
        {
            List<Showing> showings = new List<Showing>();

            DateTime today = new DateTime();
            today = DateTime.Now;

            //create the SQL string for the listings for today
            string sql = $"Select * from showing where movie_id = @movieId AND show_dateTime > GETDATE() ORDER BY show_dateTime";

            //connect to DB & execute query
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //open the connection
                    conn.Open();

                    //create the Sql cmd
                    SqlCommand cmd = new SqlCommand(sql, conn);

                    //bind the parameters
                    cmd.Parameters.AddWithValue("@movieId", movieId);

                    //execute the statement and get a reader
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        //Movie m = new Movie();
                        Showing showing = RowToObject(reader);

                        showings.Add(showing);

                        //if (DateTime.Compare(showing.ShowTimeDateTime, today) > 0)
                        //    //showing.ShowTimeDateTime > today)
                        //{
                        //    showings.Add(showing);

                        //    //showings[today] = daysShowings;
                        //}
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
        return showings;
        }

        private Showing RowToObject(SqlDataReader reader)
        {
            Showing showing = new Showing(movie.MovieId);

            showing.ShowingId = Convert.ToInt32(reader["showing_id"]);
            showing.ShowDateTime = Convert.ToDateTime(reader["show_dateTime"]);
            showing.MovieId = Convert.ToInt32(reader["movie_id"]);
            
            return showing;
        }

        public int InsertShowing(Showing showing) 
        //returns an int that is the showing id (@@IDENTITY IN THE SQL STATEMNT)
        {
            int showingId = 0; 
            string sql = $"INSERT INTO showing (show_dateTime, movie_id) VALUES (@showDateTime, @movieId); SELECT @@IDENTITY";

            //connect to DB & execute query
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //open the connection
                    conn.Open();

                    //create the SQL command
                    SqlCommand cmd = new SqlCommand();

                    cmd.Parameters.AddWithValue("@showDateTime", showing.ShowDateTime);
                    cmd.Parameters.AddWithValue("@movieId", showing.MovieId);

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Connection = conn;

                    //execute the command
                    showingId = Convert.ToInt32(cmd.ExecuteScalar());

                    //close the connection
                    conn.Close();

                    //referenced the following link:
                    // https://docs.microsoft.com/en-us/visualstudio/data-tools/insert-new-records-into-a-database?view=vs-2019
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return showingId;
        }
    }
}
