﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.DAO;
using Capstone.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace Capstone.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ShowingController : ControllerBase
    {
        private readonly IShowingDAO showingDAO;

        //create constructor
        public ShowingController(IShowingDAO _showingDAO)
        {
            showingDAO = _showingDAO;
        }

        [HttpGet("{movieId}")]
        public List<Showing> GetMovieShowings(int movieId)
        {
            return showingDAO.GetMovieShowings(movieId);
        }

        [HttpPost]
        public int InsertShowing(Showing showing)
        {
            return showingDAO.InsertShowing(showing);
        }
    }
}
