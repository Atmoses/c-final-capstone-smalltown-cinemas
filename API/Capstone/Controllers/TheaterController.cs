﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.DAO;
using Capstone.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Capstone.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TheaterController
    {
        private readonly ITheaterDAO theaterDAO;

        //create constructor
        public TheaterController(ITheaterDAO _theaterDAO)
        {
            theaterDAO = _theaterDAO;
        }
    }
}
