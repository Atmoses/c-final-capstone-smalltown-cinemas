﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.DAO;
using Capstone.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Capstone.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketDAO ticketDAO;

        //create constructor
        public TicketController(ITicketDAO _ticketDAO)
        {
            ticketDAO = _ticketDAO;
        }

        [HttpGet("{showingId}")]
        public List<Ticket> GetAvailableTickets(int showingId)
        {
            return ticketDAO.GetAvailableTickets(showingId);
        }

        [HttpPost]
        public ActionResult<bool> PurchaseTickets(Ticket ticket)
        {
            ticketDAO.PurchaseTickets(ticket);
            return StatusCode(201);
        }
        //[HttpPost]
        //public bool PurchaseTickets(Ticket ticket)
        //{
        //    return ticketDAO.PurchaseTickets(ticket);
        //}
    }
}
