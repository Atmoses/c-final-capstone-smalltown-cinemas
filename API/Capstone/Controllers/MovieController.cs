﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.DAO;
using Capstone.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Capstone.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieDAO movieDAO;
        private readonly IShowingDAO showingDAO;

        //create constructor
        public MovieController(IMovieDAO _movieDAO, IShowingDAO _showingDAO)
        {
            movieDAO = _movieDAO;
            showingDAO = _showingDAO;
        }

        [HttpGet]
        public List<Movie> GetMovies()
        {
            return movieDAO.GetMovies();
        }

        [HttpGet("{id}")]
        public Movie GetTheaterMovie(int id)
        {
            return movieDAO.GetTheaterMovie(id);
        }

        [HttpGet("import")]
        public List<Movie> ImportMovies()
        {
            return MovieLoader.ImportMovieInfo(showingDAO, movieDAO);
        }
        
        [HttpPost]    
            public int InsertMovie(Movie movie)
        {
            return movieDAO.InsertMovie(movie);
        }

    }
}
